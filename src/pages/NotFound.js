import Banner from "../components/Banner";

export default function NotFound(){
    
    let props = {
        h1: <h1 className="display-4">Page not found</h1>,
        p: <p className="lead">Go back to the <a href="/">homepage</a>.</p>
    }

    return(
        <>
            <Banner bannerProp={ props }/>
        </>
    )
}