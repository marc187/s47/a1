import { useEffect, useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { Form, Button, Row, Col, Container } from "react-bootstrap";
import UserContext from "../UserContext"

export default function Login(){
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)

    const { state, dispatch } = useContext(UserContext)

    const navigate = useNavigate()

    useEffect(() => {
        if(email !== "" && password !== ""){
            setIsDisabled(false)
        }else{
            setIsDisabled(true)
        }

    }, [email, password])

    const loginUser = (e) => {
        e.preventDefault()
        fetch(`https://salty-spire-31577.herokuapp.com/api/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email, password
            })
        })
        .then(result => result.json())
        .then(result => {
            console.log(result)
            if(result){
                localStorage.setItem(`token`, result.token)
                const token = localStorage.getItem(`token`)
    
                
              
                fetch(`https://salty-spire-31577.herokuapp.com/api/users/profile`, {
                    method: "GET",
                    headers: {
                        "Authorization": `Bearer ${token}`
                    }
                })
                .then(result => result.json())
                .then(result => {
                    localStorage.setItem(`admin`, result.isAdmin)
                    dispatch({type: "USER", payload: true})
                    

                    
                })
                setEmail("")
                setPassword("")
                navigate(`/`)
            }else{
                alert(`Incorrect credential.`)
            }
        })
    }

    return(
        <Container className="m-5 mx-auto">
           <h1 className="text-center">Login</h1>
           <Row className="justify-content-center">
               <Col xs={12} md={6}>
                    <Form onSubmit={(e) => loginUser(e)}>
                      
                      <Form.Group className="mb-3">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                      </Form.Group>

                      <Form.Group className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                      </Form.Group>

                      <Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
                    </Form>        
               </Col>
           </Row>
        </Container>
    )
}