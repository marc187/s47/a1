import { useContext, useEffect, useState } from "react";
import { Button, Container, Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";

export default function AdminView(){

    const { dispatch } = useContext(UserContext)

    const [allCourses, setAllCourses] = useState()

    const fetchData = () => {
        fetch(`https://salty-spire-31577.herokuapp.com/api/courses`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem(`token`)}`
            }
        })
        .then(result => result.json())
        .then(result => {
            console.log(result)

            dispatch({type: "USER", payload: true})

            setAllCourses( result.map(course => {
                console.log(result)
                return(
                <tr key={course._id}>
                    <td>{course._id}</td>
                    <td>{course.courseName}</td>
                    <td>{course.price}</td>
                    <td>{course.isOffered ? "Active" : "Inactive"}</td>
                    <td>
                        {
                            course.isOffered ?
                                <Button className="btn btn-danger" onClick={ () => handleArchive(course._id) }> Archive</Button>
                            :
                                <>
                                    <Button className="btn btn-success mr-3" onClick={ () => handleUnArchive(course._id) }>Unarchive</Button>
                                    <Button className="btn btn-secondary"  onClick={ () => handleDelete(course._id) }>Delete</Button>
                                </>
                        }
                    </td>
                </tr>
                )
            }))
        })
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleArchive = (courseId) => {
        console.log(courseId)
        fetch(`https://salty-spire-31577.herokuapp.com/api/courses/${courseId}/archive`, {
            method: "PATCH",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem(`token`)}`
            }
        })
        .then(result => result.json())
        .then(result => {
            console.log(result)
            if(result){
                alert(`Successfully archive the course.`)
                fetchData()
            }
        })
    }

    const handleUnArchive = (courseId) => {
        fetch(`https://salty-spire-31577.herokuapp.com/api/courses/${courseId}/unArchive`, {
            method: "PATCH",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem(`token`)}`
            }
        })
        .then(result => result.json())
        .then(result => {
            console.log(result)
            if(result){
                alert(`Successfully unarchive the course.`)
                fetchData()
            }
        })
    }

    const handleDelete = (courseId) => {
        fetch(`https://salty-spire-31577.herokuapp.com/api/courses/${courseId}/delete-course`, {
            method: "DELETE",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem(`token`)}`
            }
        })
        .then(result => result.json())
        .then(result => {
            console.log(result)
            if(result){
                alert(`Successfully deleted the course.`)
                fetchData()
            }
        })
    }

    return(
        <Container className="container">
            <h1 className="my-5 text-center">
                Course Dashboard
            </h1>
            <div className="text-right">
                <Link className="btn btn-info m-2" to={`/addCourse`}>Add Course</Link>
                <Link className="btn btn-info m-2" to={`/access`}>User Dashboard</Link>
            </div>
           
            <Table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Course Name</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    { allCourses }                   
                </tbody>
            </Table>
        </Container>
    )
}