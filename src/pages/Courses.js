import CourseCard from "../components/CourseCard"
import { useContext, useEffect, useState } from "react"
import UserContext from "../UserContext"
import AdminView from "./AdminView"

const admin = localStorage.getItem(`admin`)
const token = localStorage.getItem(`token`)

export default function Courses(){

    const { state, dispatch } = useContext(UserContext)
    console.log(state)

    const [courses, setCourses] = useState([])
    
    useEffect( () => {
        if(admin === "false" || admin === null){
            fetch(`https://salty-spire-31577.herokuapp.com/api/courses/isActive`, {
                method: "GET",
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            })
            .then(result => result.json())
            .then(result => {
                console.log(result)

                if(token !== null){
                    dispatch({type: "USER", payload: true})
                }
                setCourses(result.map( course => {
                    return <CourseCard key={course._id} courseProp={course}/>
                }))
                
            })
        }
    }, [])


    return(
        <>
            {
                admin === "false" || admin === null ?
                <>{courses}</> 
                : 
                <><AdminView /></>
            }
        </>
    )
}