import { useEffect, useState, useContext } from "react";
import { Form, Button, Row, Col, Container } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import UserContext from "../UserContext"


export default function Register(){

    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [conPassword, setConPassword] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)

    const navigate = useNavigate()

    const { user, setUser } = useContext(UserContext)

    // useEffect(function, options)
    useEffect(() => {
        // console.log('render')

        if((firstName !== "" && lastName !== "" && email !== "" && password !== "" && conPassword !== "") && (password == conPassword)){
            setIsDisabled(false)
        } else {
            setIsDisabled(true)
        }
        
    }, [firstName, lastName, email, password, conPassword])

    const registerUser = (e) => {
        e.preventDefault()
        
        fetch(`https://salty-spire-31577.herokuapp.com/api/users/email-exists`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email
            })
        })
        .then(result => result.json())
        .then(result => {
            if(!result){
                fetch(`https://salty-spire-31577.herokuapp.com/api/users/register`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        firstName, lastName, email, password
                    })
                })
                .then(result => result.json())
                .then(result => {
                    alert(`Account registered succssfully.`)
                    navigate(`/login`)
                })
            }else{
                alert(`Email already exist.`)
            }
        })
    }

    
    return(
        <Container className="m-5 mx-auto">
           <h1 className="text-center">Register</h1>
           <Row className="justify-content-center">
               <Col xs={12} md={6}>
                    <Form onSubmit={(e) => registerUser(e)}>
                      <Form.Group className="mb-3">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control type="text" value={firstName} onChange={(e) => setFirstName(e.target.value)}/>
                      </Form.Group>

                      <Form.Group className="mb-3">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control type="text" value={lastName} onChange={(e) => setLastName(e.target.value)}/>
                      </Form.Group>

                      <Form.Group className="mb-3">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                      </Form.Group>

                      <Form.Group className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                      </Form.Group>

                      <Form.Group className="mb-3">
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control type="password" value={conPassword} onChange={(e) => setConPassword(e.target.value)}/>
                      </Form.Group>

                      <Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
                    </Form>        
               </Col>
           </Row>
        </Container>
    )
}