import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import { useContext, useEffect, useState } from "react";
import UserContext from "../UserContext";
import { useNavigate } from 'react-router-dom';

const token = localStorage.getItem(`token`)

export default function CreateCourse(){

    const [courseName, setCourseName] = useState("")
    const [price, setPrice] = useState(0)
    const [description, setDescription] = useState("")

    const navigate = useNavigate()

    const { dispatch } = useContext(UserContext)

    useEffect( () => {
        if(token !== null){
            dispatch({type: "USER", payload: true})
        }
    }, [])


    // update the user state using context
	// add the course by getting the user input and send it as a client request via fetch
	// send an alert once added course succesfully
	// navigate back to courses dashboard
	// course newly added must show in the dashboard at the very end of the courses

    const handleAddCourse = (e) => {
       e.preventDefault()

       fetch(`https://salty-spire-31577.herokuapp.com/api/courses/create`, {
           method: "POST",
           headers: {
               "Content-Type": "application/json",
               "Authorization": `Bearer ${token}`
           },
           body: JSON.stringify({
               courseName,
               description,
               price
           })
       })
       .then(result => result.json())
       .then(result => {
            if(result){
                alert(`Successfully added course.`)

                navigate(`/courses`)
            }
       })
    }

	return(
		<Container className="container m-5">
		 	<h1 className="text-center">Add Course</h1>
			<Form onSubmit={ (e) => handleAddCourse(e)}>
				<Row>
					<Col xs={12} md={8}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Course Name"
					    		type="text" 
					    		value={courseName}
                                onChange={ (e) => setCourseName(e.target.value)}
					    		
					    	/>
						</Form.Group>
					</Col>
					<Col xs={12}  md={4}>
						<Form.Group className="mb-3">
					    	<Form.Control
                                placeholder='Course Price'
					    		type="number" 
					    		value={price}
                                onChange={ (e) => setPrice(e.target.value)}
					    		
					    	/>
						</Form.Group>
					</Col>
				</Row>
				<Row>
					<Col xs={12}  md={12}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Course Description"
					    		type="text" 
					    		value={description}
                                onChange={ (e) => setDescription(e.target.value)}
					    		
					    	/>
						</Form.Group>
					</Col>
				</Row>
				<Button type='submit' className="btn btn-info btn-block">Add Course</Button>
			</Form>
		</Container>
	)
}