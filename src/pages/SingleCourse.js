import { Col, Container, Row, Button, Card } from "react-bootstrap";
import { useContext, useEffect, useState } from "react";
import UserContext from "../UserContext";
import { useNavigate, useParams } from "react-router-dom";
const token = localStorage.getItem(`token`)

export default function SingleCourse(){

    const { dispatch } = useContext(UserContext)

    const { courseId } = useParams()
    console.log(courseId)

    const navigate = useNavigate()

    const [ courseName, setCourseName ] = useState("")
    const [ description, setDescription ] = useState("")
    const [ price, setPrice ] = useState(0)

    const fetchCourses = () => {
        fetch(`http://localhost:3000/api/courses/${courseId}`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(response => {
            console.log(response)

            setCourseName(response.courseName)
            setDescription(response.description)
            setPrice(response.price)

        })
    }

    useEffect(() => {
        
        if(token !== null){
            dispatch({type: "USER", payload: true})
        }

        fetchCourses()
    }, [])

    const handleEnroll = (courseId) => {
        console.log(`enroll`)

        fetch(`https://salty-spire-31577.herokuapp.com/api/users/enroll`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({
                courseId: courseId
            })
        })
        .then(response => response.json())
        .then(response => {
            if(response){
                alert(`Thank you for enrolling to this course!`)
                navigate(`/courses`)
            }
        })
    }
    
    return(
        <Container className="container">
            <Row className="justify-content-center">
                <Col xs={12} md={6}>
                    <Card className='m-5'>
                        <Card.Body>
                            <Card.Title>{courseName}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>
                                {description}
                            </Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>{price}</Card.Text>
                            
                            <Button className="btn btn-info" onClick={ () => handleEnroll(courseId) }>Enroll</Button>
                        </Card.Body>
                    </Card>
                    </Col>
            </Row>
        </Container>
    )
}