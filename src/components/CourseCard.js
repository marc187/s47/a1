import { useEffect, useState } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}){
  const {courseName, description, price, _id} = courseProp  

    let [count, setCount] = useState(0)
    let [seat, setSeat] = useState(30)

    const handleClick = () => {
      if(seat > 0){
        setCount(count + 1)
        setSeat(seat - 1)
      }else{
        alert(`No more seats.`)
      }
    }

    return(
        <Card className='m-5'>
            <Card.Body>
              <Card.Title>{courseName}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>
              {description}
              </Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{price}</Card.Text>
              <Link to={`/courses/${_id}`}> Check course </Link>
              <Card.Text>Count: {count}</Card.Text>
              <Card.Text>{count} Enrollees</Card.Text>
              <Button variant="primary" onClick={handleClick}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}