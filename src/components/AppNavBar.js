import { Navbar, Nav, Container } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useContext, useEffect } from 'react';

const token = localStorage.getItem(`token`)

export default function AppNavBar(){

    const { state, dispatch } = useContext(UserContext)
    console.log(state)

    useEffect( () => {
      if(token){
        dispatch({type: "USER", payload: true})
      }else{
        dispatch({type: "USER", payload: null})
      }
    }, [])

    const NavLinks = () => {
        if(state === true){
          return(
            <>
              <Nav.Link className="text-light" href="/logout">Logout</Nav.Link>
            </>
          )
        }else{
          return(
            <>
              <Nav.Link className="text-light" href="/login">Login</Nav.Link>
              <Nav.Link className="text-light" href="/register">Sign Up</Nav.Link>
            </>
          )
        }
    }

    return(
        <Navbar bg="info" expand="lg">
          <Container>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link className="text-light" href="/">Home</Nav.Link>
                <Nav.Link className="text-light" href="/courses">Course</Nav.Link>
                <NavLinks/>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar> 
    )
}