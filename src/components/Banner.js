
export default function Banner({ bannerProp }){
    const {h1, p, btn} = bannerProp
    return(
        <div className="jumbotron jumbotron-fluid">
            <div className="container">
                { h1 }
                { p }
                { btn }
            </div>
        </div>        
    )
}