import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import { useEffect, useReducer } from "react";
import { UserProvider } from "./UserContext"
import { initialState, reducer } from "./reducer/UserReducer";

import Home from "./pages/Home";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";

import AppNavBar from "./components/AppNavBar";
import Footer from "./components/Footer";
import Logout from "./pages/logout";
import NotFound from "./pages/NotFound";
import SingleCourse from "./pages/SingleCourse";
import CreateCourse from "./pages/CreateCourse";


function App() {

  const [ state, dispatch ] = useReducer(reducer, initialState)
  console.log(state)

  return(
    <UserProvider value={{ state, dispatch }}>
      <BrowserRouter>
        <AppNavBar/>
        <Routes>
          <Route path="/" element={ <Home/> }/>
          <Route path="/courses" element={ <Courses/> }/>
          <Route path="/courses/:courseId" element={ <SingleCourse/> }/>
          <Route path="/register" element={ <Register/> }/>
          <Route path="/login" element={ <Login/> }/>
          <Route path="/logout" element={ <Logout/> }/>
          <Route path="/addCourse" element={ <CreateCourse/> }/>
          <Route path="*" element={ <NotFound/> }/>
        </Routes>
        <Footer/>
      </BrowserRouter>
    </UserProvider>
  )
}

export default App;
